# How to install on Firefox

1. Open about:config and set "security.csp.enable" to false
2. Go to a script and click on the Raw button. 
This should, for example, take you to: https://gitlab.com/rooiratel/userscripts/raw/master/scripts/hn_planification.user.js

3. Click install
4. Go back to about:config and re-enable csp.

This workaround is needed becuase of this bug: https://bugzilla.mozilla.org/show_bug.cgi?id=1411641 
