// ==UserScript==
// @name         MM plainification
// @namespace    https://gitlab.com/rooiratel/userscripts
// @version      0.2
// @description  make maroelamedia background a single colour and remove bg image
// @author       Rooiratel
// @match        https://maroelamedia.co.za/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    document.body.style.backgroundImage = "url('')";
    document.body.style.backgroundColor = '#f5f2ed';
})();
