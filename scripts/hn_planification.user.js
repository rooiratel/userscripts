// ==UserScript==
// @name         HN plainification
// @namespace    https://gitlab.com/rooiratel/userscripts
// @version      0.2
// @description  make HN background a single colour
// @author       Rooiratel
// @match        https://news.ycombinator.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    document.getElementById('hnmain').setAttribute("bgcolor", 'F9F9F9');
    document.body.style.background = '#F9F9F9';
})();
