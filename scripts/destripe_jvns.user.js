// ==UserScript==
// @name         Remove Orange stripe from jvns
// @namespace    https://gitlab.com/rooiratel/userscripts
// @version      0.2
// @description  Removes the orange stripe on the righthand side of the blog, on jvns.ca
// @author       Rooiratel
// @match        *://jvns.ca/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    document.getElementById('main').style['border-right-width'] = 0;
})();
